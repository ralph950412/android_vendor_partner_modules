//
// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "vendor_unbundled_google_modules_ArtGooglePrebuilt_license",
    ],
}

license {
    name: "vendor_unbundled_google_modules_ArtGooglePrebuilt_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-BSD",
        "SPDX-license-identifier-GPL-2.0",
        "SPDX-license-identifier-ISC",
        "SPDX-license-identifier-MIT",
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type {
    name: "art_prebuilt_apex_set",
    module_type: "apex_set",
    config_namespace: "art_module",
    bool_variables: ["source_build"],
    properties: ["prefer"],
}

art_prebuilt_apex_set {
    name: "com.google.android.art",
    apex_name: "com.android.art",
    owner: "google",
    // Override both AOSP APEX variants, to ensure only com.google.android.art
    // is installed regardless which APEX the logic in runtime_libart.mk has
    // picked.
    overrides: [
        "com.android.art",
        "com.android.art.debug",
    ],
    filename: "com.google.android.art.apex",
    set: "com.google.android.art.apks",
    // Do not prefer prebuilt if SOONG_CONFIG_art_module_source_build is true.
    // That implies prefer:false also when MODULE_BUILD_FROM_SOURCE is true.
    prefer: true,
    soong_config_variables: {
       source_build: {
           prefer: false
       }
    },
    // Make fragment related files from the apex file available for use by the
    // build when using prebuilts, e.g. for running the boot jars package check
    // and hidden API flag validation among other uses.
    exported_bootclasspath_fragments: ["art-bootclasspath-fragment"],
    exported_systemserverclasspath_fragments: ["art-systemserverclasspath-fragment"],
}


art_prebuilt_apex_set {
    name: "com.google.android.art_compressed",
    apex_name: "com.android.art",
    owner: "google",
    // Override both AOSP APEX variants, to ensure only com.google.android.art
    // is installed regardless which APEX the logic in runtime_libart.mk has
    // picked.
    overrides: [
        "com.android.art",
        "com.android.art.debug",
    ],
//    filename: "com.google.android.art.capex",
    set: "com.google.android.art_compressed.apks",
    // Do not prefer prebuilt if SOONG_CONFIG_art_module_source_build is true.
    // That implies prefer:false also when MODULE_BUILD_FROM_SOURCE is true.
    prefer: true,
    soong_config_variables: {
       source_build: {
           prefer: false
       }
    },
    exported_bootclasspath_fragments: ["art-bootclasspath-fragment"],
    exported_systemserverclasspath_fragments: ["art-systemserverclasspath-fragment"],
}
