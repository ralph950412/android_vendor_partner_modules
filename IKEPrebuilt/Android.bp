//
// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "vendor_unbundled_google_modules_IpSecGooglePrebuilt_license",
    ],
}

license {
    name: "vendor_unbundled_google_modules_IpSecGooglePrebuilt_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-MIT",
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_apex_set"],
}

module_apex_set {
    name: "com.google.android.ipsec",
    apex_name: "com.android.ipsec",
    required: [
        "android.net.ipsec.ike.com.android.ipsec",
    ],
    owner: "google",
    overrides: ["com.android.ipsec"],
    filename: "com.google.android.ipsec.apex",
    set: "com.google.android.ipsec.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    exported_bootclasspath_fragments: ["com.android.ipsec-bootclasspath-fragment"],
}

module_apex_set {
    name: "com.google.android.ipsec_compressed",
    apex_name: "com.android.ipsec",
    required: [
        "android.net.ipsec.ike.com.android.ipsec",
    ],
    owner: "google",
    overrides: ["com.android.ipsec"],
//    filename: "com.google.android.ipsec.capex",
    set: "com.google.android.ipsec_compressed.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    exported_bootclasspath_fragments: ["com.android.ipsec-bootclasspath-fragment"],
}
